# InternProject-WebUsingCI

This is my project when I was an intern student

## Responsibilities:

- 👥 : Work as a team to build an interactive website
- 👨‍💼 : Understanding Agile Methodologies using Scrum and Become

## Scrum Master

- 👨‍💻 : Understanding how CodeIgniter works

## How to Start :

- Install CodeIgniter 4
- Install Composer
- Make duplicate the env file become .env file
- Uncomment and fill up the necessery thing

## How to Run :

Install

- Composer
- CodeIgniter versi 4
- XAMPP and PHP

Set up `env` to `.env` and fill the necessary things

run on terminal

```
php spark serve
```
